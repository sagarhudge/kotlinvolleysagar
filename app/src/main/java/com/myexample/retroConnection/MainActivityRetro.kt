package com.myexample.retroConnection

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.myexample.Adapter.MyListAdapter
import com.myexample.Helper.DatabaseHandler
import com.myexample.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main_retro.*
import synnex.sagar.retro.Api
import synnex.sagar.retro.Response.Category
import synnex.sagar.retro.Response.Example

class MainActivityRetro : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_retro)
        val btnGetData = findViewById<Button>(R.id.btnHitApi)
        btnGetData?.setOnClickListener {
            loadData();
        }

        val btnShow = findViewById<Button>(R.id.btnLoadData)
        btnShow?.setOnClickListener {
            showData();
        }
    }

    private fun showData() {
        //creating the instance of DatabaseHandler class
        val databaseHandler: DatabaseHandler= DatabaseHandler(this)
        //calling the viewEmployee method of DatabaseHandler class to read the records
        val emp: List<Category> = databaseHandler.viewEmployee()
        val empArrayId = Array<String>(emp.size){"0"}
        val empArrayName = Array<String>(emp.size){"null"}
        val empArrayEmail = Array<String>(emp.size){"null"}
        var index = 0
        for(e in emp){
            empArrayId[index] = e.userId.toString()
            empArrayName[index] = e.userName
            index++
        }
        //creating custom ArrayAdapter
        val myListAdapter = MyListAdapter(this,empArrayId,empArrayName,empArrayEmail)
        listView.adapter = myListAdapter
        listView.setOnItemClickListener { parent, view, position, id ->


        }
    }

    companion object {
        private var api: Api? = null
    }
    private fun loadData(){
        api = Api.start()
        api!!.apiGetExample
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<Example>() {
                    override fun onNext(response: Example) {
                        // response
                        populateResponse(response)
                    }
                    override fun onError(e: Throwable) {
                        Log.e("printStackTrace",""+e.message); }
                    override fun onComplete() {
                    }
                })
    }
    private fun populateResponse(response: Example) {

        var i =0
        for (id in response.categories!!) {
            try {
                val id = response.categories!![i].id
                val name = response.categories!![i].name
                val databaseHandler: DatabaseHandler = DatabaseHandler(this)
                if (name!!.trim() != "") {
                    val status = databaseHandler.addEmployee(Category(id!!, name!!))
                    if (status > -1) {
                        //Toast.makeText(applicationContext, "record save", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(applicationContext, "id or name or email cannot be blank", Toast.LENGTH_LONG).show()
                }
                i++
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        Toast.makeText(applicationContext, "Data Saved", Toast.LENGTH_LONG).show()
        showData()
    }
}
