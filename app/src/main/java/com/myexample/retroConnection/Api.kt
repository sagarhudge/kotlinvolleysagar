package synnex.sagar.retro

import com.google.gson.GsonBuilder

import java.util.concurrent.TimeUnit

import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import synnex.sagar.retro.Response.Example

class Api private constructor() {
    private val getNetworkIterface: INetworkInterface
    val apiGetExample: Observable<Example>
        get() = getNetworkIterface.exampleObservable
    init {
        val gson = GsonBuilder().setLenient().create()
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()
        val retrofit = Retrofit.Builder().client(okHttpClient).baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson)).build()
        getNetworkIterface = retrofit.create(INetworkInterface::class.java)
    }
    companion object {
        private val BASE_URL = "https://stark-spire-93433.herokuapp.com/"
        private var instance: Api? = null
        fun start(): Api { if (instance == null)
                        return Api() else return instance as Api }
    }
}
