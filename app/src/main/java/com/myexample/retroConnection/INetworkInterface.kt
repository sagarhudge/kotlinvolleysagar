package synnex.sagar.retro


import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import synnex.sagar.retro.Response.Example

interface INetworkInterface {

    @get:Headers("Content-Type: application/json")
    @get:GET("/json")
    val exampleObservable: Observable<Example>
}
