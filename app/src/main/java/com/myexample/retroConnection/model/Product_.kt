package synnex.sagar.retro.Response

import com.google.gson.annotations.SerializedName

class Product_ {

    @SerializedName("id")
    @get:SerializedName("id")
    @set:SerializedName("id")
    var id: Int? = null
    @SerializedName("view_count")
    @get:SerializedName("view_count")
    @set:SerializedName("view_count")
    var viewCount: Int? = null
    @SerializedName("order_count")
    @get:SerializedName("order_count")
    @set:SerializedName("order_count")
    var orderCount: Int? = null
    @SerializedName("shares")
    @get:SerializedName("shares")
    @set:SerializedName("shares")
    var shares: Int? = null

}
