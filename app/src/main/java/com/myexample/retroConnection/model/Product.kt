package synnex.sagar.retro.Response

import com.google.gson.annotations.SerializedName

class Product {
    @SerializedName("id")
    @get:SerializedName("id")
    @set:SerializedName("id")
    var id: Int? = null
    @SerializedName("name")
    @get:SerializedName("name")
    @set:SerializedName("name")
    var name: String? = null
    @SerializedName("date_added")
    @get:SerializedName("date_added")
    @set:SerializedName("date_added")
    var dateAdded: String? = null
    @SerializedName("variants")
    @get:SerializedName("variants")
    @set:SerializedName("variants")
    var variants: List<Variant>? = null
    @SerializedName("tax")
    @get:SerializedName("tax")
    @set:SerializedName("tax")
    var tax: Tax? = null
}
