package synnex.sagar.retro.Response

import com.google.gson.annotations.SerializedName

class Variant {

    @SerializedName("id")
    @get:SerializedName("id")
    @set:SerializedName("id")
    var id: Int? = null
    @SerializedName("color")
    @get:SerializedName("color")
    @set:SerializedName("color")
    var color: String? = null
    @SerializedName("size")
    @get:SerializedName("size")
    @set:SerializedName("size")
    var size: Any? = null
    @SerializedName("price")
    @get:SerializedName("price")
    @set:SerializedName("price")
    var price: Int? = null
}
