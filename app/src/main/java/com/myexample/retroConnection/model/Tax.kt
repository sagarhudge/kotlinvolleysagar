package synnex.sagar.retro.Response

import com.google.gson.annotations.SerializedName

class Tax {

    @SerializedName("name")
    @get:SerializedName("name")
    @set:SerializedName("name")
    var name: String? = null
    @SerializedName("value")
    @get:SerializedName("value")
    @set:SerializedName("value")
    var value: String? = null
}
