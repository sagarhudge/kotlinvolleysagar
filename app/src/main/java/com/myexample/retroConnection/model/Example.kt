package synnex.sagar.retro.Response

import com.google.gson.annotations.SerializedName

class Example {
    @SerializedName("categories")
    @get:SerializedName("categories")
    @set:SerializedName("categories")
    var categories: List<Category>? = null
    @SerializedName("rankings")
    @get:SerializedName("rankings")
    @set:SerializedName("rankings")
    var rankings: List<Ranking>? = null
}
