package synnex.sagar.retro.Response

import com.google.gson.annotations.SerializedName

class Category (var userId: Int, val userName:String){

    @SerializedName("id")
    @get:SerializedName("id")
    @set:SerializedName("id")
    var id: Int? = null
    @SerializedName("name")
    @get:SerializedName("name")
    @set:SerializedName("name")
    var name: String? = null
    @SerializedName("products")
    @get:SerializedName("products")
    @set:SerializedName("products")
    var products: List<Product>? = null
    @SerializedName("child_categories")
    @get:SerializedName("child_categories")
    @set:SerializedName("child_categories")
    var childCategories: List<Int>? = null
}
