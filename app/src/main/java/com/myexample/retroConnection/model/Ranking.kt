package synnex.sagar.retro.Response

import com.google.gson.annotations.SerializedName

class Ranking {

    @SerializedName("ranking")
    @get:SerializedName("ranking")
    @set:SerializedName("ranking")
    var ranking: String? = null
    @SerializedName("products")
    @get:SerializedName("products")
    @set:SerializedName("products")
    var products: List<Product_>? = null
}
