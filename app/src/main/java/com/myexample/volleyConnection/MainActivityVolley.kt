package com.myexample.volleyConnection

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.myexample.R
import com.myexample.requests.JSONArrayRequest
import com.myexample.requests.JSONObjectRequest
import com.myexample.requests.VolleyRequestQueue
import com.myexample.requests.VolleyRequestQueue.Companion.REQUEST_TAG
import org.json.JSONArray
import org.json.JSONObject

class MainActivityVolley : AppCompatActivity(), Response.Listener<JSONArray>, Response.ErrorListener {
    override fun onResponse(response: JSONArray?) {
        Log.e("response",""+response.toString())
    }

    override fun onErrorResponse(error: VolleyError?) {
    }

    var requestQueue: RequestQueue? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        requestQueue = VolleyRequestQueue.getInstance(this).getRequestQueue();
        val url:String="http://dummy.restapiexample.com/api/v1/employees"

      val lockatedJSONObjectRequest : JSONArrayRequest = JSONArrayRequest(Request.Method.GET,
              url, null, this, this);
        lockatedJSONObjectRequest.setTag(REQUEST_TAG+"GET")
        requestQueue!!.add(lockatedJSONObjectRequest)

        onPostData()
    }

    private fun onPostData() {

       val jsonObject: JSONObject  =    JSONObject()
        jsonObject.put("name", ""+System.currentTimeMillis()+"")
                    jsonObject.put("age", "22")
                    jsonObject.put("salary", "22888")

        try {
            val url = "http://dummy.restapiexample.com/api/v1/create"
            requestQueue = VolleyRequestQueue.getInstance(this).getRequestQueue();

            val lockatedJSONObjectRequest = JSONObjectRequest(Request.Method.POST,
                    url, jsonObject, Response.Listener { response ->
                Log.d("TAGTest", response.toString())

            }, Response.ErrorListener { error ->
                Log.d("TAGTest", error.message)

                // Error in request
            })
            lockatedJSONObjectRequest.setTag(REQUEST_TAG+"POST")
            requestQueue!!.add(lockatedJSONObjectRequest)

        } catch (e: Exception) {
            e.printStackTrace()
        }


    }


}
