package com.myexample.requests

import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.toolbox.JsonObjectRequest

import org.json.JSONObject

import java.util.HashMap

class JSONObjectRequest(method: Int, url: String, jsonRequest: JSONObject?, listener:

Response.Listener<JSONObject>, errorListener: Response.ErrorListener) : JsonObjectRequest(method,
        url, jsonRequest, listener, errorListener) {

    @Throws(AuthFailureError::class)
    override fun getHeaders(): Map<String, String> {
        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/json; charset=utf-8"
        return headers
    }


    override fun getBodyContentType(): String {
        return "application/x-www-form-urlencoded; charset=UTF-8"
    }

    override fun getRetryPolicy(): RetryPolicy {
        return super.getRetryPolicy()
    }
}
