package com.myexample.requests

import android.annotation.SuppressLint
import android.content.Context
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import org.json.JSONObject
import java.util.*

class VolleyRequestQueue private constructor(context: Context) {
    private var mRequestQueue: RequestQueue? = null
    private val mImageLoader: ImageLoader? = null
    var JSONObjectRequest: JSONObjectRequest? = null
        private set

    internal lateinit var requestQueue: RequestQueue

    init {
        mContext = context
        mRequestQueue = getRequestQueue()
    }

    fun sendRequest(tag: String, method: Int, url: String, jsonObject: JSONObject,
                    listener: Response.Listener<JSONObject>, errorListener: Response.ErrorListener) {
        val socketTimeout = REQUEST_TIME_OUT
        requestQueue = mInstance!!.getRequestQueue()
        JSONObjectRequest = JSONObjectRequest(method, url, jsonObject, listener, errorListener)
        JSONObjectRequest!!.tag = tag
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        JSONObjectRequest!!.retryPolicy = policy
        requestQueue.add(JSONObjectRequest!!)
    }

    fun sendRequest(tag: String, method: Int, url: String, jsonObject: JSONObject,
                    listener: Response.Listener<String>, errorListener: Response.ErrorListener, message: String) {
        val socketTimeout = REQUEST_TIME_OUT
        requestQueue = mInstance!!.getRequestQueue()
        val sr = object : StringRequest(method, url, listener, errorListener) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["msg"] = message
                return params
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Content-Type"] = "application/x-www-form-urlencoded"
                return params
            }
        }
        sr.tag = tag
        val policy = DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        sr.retryPolicy = policy
        requestQueue.add(sr)
    }

    fun getRequestQueue(): RequestQueue {
        if (mRequestQueue == null) {
            val cache = DiskBasedCache(mContext.cacheDir, 10 * 1024 * 1024)
            val network = BasicNetwork(HurlStack())
            mRequestQueue = RequestQueue(cache, network)
            mRequestQueue!!.start()
        }
        return mRequestQueue as RequestQueue
    }

    companion object {
        var REQUEST_TIME_OUT = 1000 * 2 * 60

        @SuppressLint("StaticFieldLeak")
        private var mInstance: VolleyRequestQueue? = null
        @SuppressLint("StaticFieldLeak")
        private lateinit var mContext: Context

        val REQUEST_TAG = "API-REQUEST"

        @Synchronized
        fun getInstance(context: Context): VolleyRequestQueue {
            if (mInstance == null) {
                mInstance = VolleyRequestQueue(context)
            }
            return mInstance as VolleyRequestQueue
        }
    }

}
