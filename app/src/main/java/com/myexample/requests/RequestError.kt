package com.myexample.requests

import android.content.Context
import android.widget.Toast

import com.android.volley.AuthFailureError
import com.android.volley.NetworkError
import com.android.volley.NoConnectionError
import com.android.volley.ParseError
import com.android.volley.ServerError
import com.android.volley.TimeoutError
import com.android.volley.VolleyError

object RequestError {
    fun onRequestError(context: Context, error: VolleyError) {
        if (error is NoConnectionError) {
            Toast.makeText(context, "connection_error", Toast.LENGTH_SHORT).show()
        } else if (error is AuthFailureError) {
            Toast.makeText(context, "auth_failure_error", Toast.LENGTH_SHORT).show()
        } else if (error is ServerError) {
            Toast.makeText(context, "server_error", Toast.LENGTH_SHORT).show()

        } else if (error is NetworkError) {
            Toast.makeText(context, "network_error", Toast.LENGTH_SHORT).show()

        } else if (error is ParseError) {
            Toast.makeText(context, "parse_error", Toast.LENGTH_SHORT).show()

        } else if (error is TimeoutError) {
            Toast.makeText(context, "timeout_error", Toast.LENGTH_SHORT).show()
        }
    }
}
