package com.myexample.requests

import com.android.volley.RetryPolicy
import com.android.volley.AuthFailureError
import com.android.volley.Response
import org.json.JSONArray
import com.android.volley.toolbox.JsonArrayRequest


class JSONArrayRequest(method: Int, url: String, jsonRequest: JSONArray?, listener: Response.Listener<JSONArray>,
                       errorListener: Response.ErrorListener) : JsonArrayRequest(method, url, jsonRequest,
        listener, errorListener) {

    @Throws(AuthFailureError::class)
    override fun getHeaders(): Map<String, String> {
        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/json; charset=utf-8"
        return headers
    }

    override fun getRetryPolicy(): RetryPolicy {
        return super.getRetryPolicy()
    }

}
